public class Children extends Parents{
	public static void main(String[] args){
		Parents child=new Children();
		//Dynamic binding or Late binding we can only access parent method and Overridden methods
		//we cannot access Child method.
		child.study();
		//child.play();
		child.motivate();
		child.workHard();
		
	}
	public void play(){
		System.out.println("Cricket");
	}
	public void study(){
		System.out.println("law");
	}
}