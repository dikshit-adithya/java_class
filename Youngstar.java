package salem;
public class Youngstar extends Parent{
	public Youngstar(){
		System.out.println("youngstar class constructor");
	}
	//public Youngstar(int i){
	//	System.out.println("constructor 2:"+i);
	//}	
	public static void main(String[] args){
		Youngstar yy=new Youngstar();// this yy cannot visible to outside of the block;
		//Youngstar xx=new Youngstar(89);
		//yy acting as parent object also.
		yy.study();
		yy.gardening();
		//yy.savings();
		yy.fixMarriage();
		//System.out.println(yy.balance);
		//Parent pp=new Parent();
		//pp.fixMarriage();
		yy.takeRest();
		yy.visit();
		yy.meetRelatives();
		//super.takeRest(); super cannot be used in the static content.
	}
	public void study(){
		System.out.println("study");
	}
	public void fixMarriage(){
		System.out.println(super.balance);
		super.fixMarriage();
		System.out.println("Youngstar-opinion");
	}
}