public interface Rules{
	int noOfLeave=12;
	//by default in interface, the variable is final,we cannot reassign the value.
	//by default the interface variables are static.
	//we cannot create object in interface like abstract.
	//we cannot create constructors in interface.
public void comeOnTime();//by default it is abstract we need not to mention abstract in interface.
public void getLeave();
public void getSalary();
}