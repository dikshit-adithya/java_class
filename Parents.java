public abstract class Parents{
	//public static void main(String[] args){
	//	Parents pp=new Parents();//in abstract class cannot be create instance(object).
	//}if the class have one abstract method the class should be declared as abstract.
	public abstract void study();
	public void workHard(){
		System.out.println("Hard Working");
	}
	public void motivate(){
		System.out.println("Motivating");
	}
}