public class BookShop{
	String author;
	int price;
	int pages;
public BookShop(String author,int price,int pages){
	System.out.println("Hi i am constructor");
	this.author=author;
	this.price=price;
	this.pages=pages;
}
public static void main(String[] args){
	BookShop book1=new BookShop("abc",20,55);
	BookShop book2=new BookShop("xyz",44,89);
	BookShop book3=new BookShop("mnp",34,100);
	book1.buy();
	book2.buy();
	book3.buy();
}
	public void buy(){
		System.out.println(author+" ---"+price+"---"+pages);
							//or
		System.out.println(this.author+" ---"+this.price+"---"+this.pages);
		
		
}
}
//called automatically when object is created.
//no return datatype required.
//should have same class name.
//Initializing object specific informaton.

