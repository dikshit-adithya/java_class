public class Employee implements Rules{
	public static void main(String[] args){
		//Rules r=new Rules();we cannot create object in interface like abstract.
		//Employee emp=new Employee();
		Rules manager=new Employee();
		manager.comeOnTime();
		manager.getLeave();
		//manager.leisure();
		
		//emp.comeOnTime();
		//emp.getLeave();
		//System.out.println(emp.noOfLeave);
		//System.out.println(Employee.noOfLeave);
		//System.out.println(Rules.noOfLeave);
		
	}
	public void comeOnTime(){
		System.out.println("9 AM");
	}
	public void getLeave(){
		System.out.println("Will inform");
	}

	public void getSalary(){
		System.out.println("on 1st every month");
	}
	public void leisure(){
		System.out.println("chating");
	}
}