public class Home{
	// static refers to class specification information.
	static String name;
	static int age=15;
	//non static refers to instance(object) information.
	String name1;
	int age1;
	float weight1;
	boolean student1;
	char lastname1;
public static void main(String[] args){
	//the variable declared inside the block or method is called local variable.
	//the variable declared outside the block or method is called global variable or fields.
	int no=10;
	System.out.println("Welcome to java world");
	System.out.println("Home name:"+Home.name);
	System.out.println("Home age:"+Home.age);
	System.out.println("number:"+no);
	
	Home person1=new Home();//object(instance)
	System.out.println("person1 name:"+person1.name1);
	System.out.println("person1 age:"+person1.age1);
	System.out.println("person1 weight:"+person1.weight1);
	System.out.println("person1 student:"+person1.student1);
	System.out.println("person1 lastname:"+person1.lastname1);
}
}