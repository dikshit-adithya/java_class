package salem;
public class Parent extends GrandParent{
	public Parent(){
		System.out.println("Parent class constructor");
	}
	public int balance=1500;
	private void savings(){
		System.out.println("savings-10000");
	}
	public void fixMarriage(){
		super.fixMarriage();
		System.out.println("for their child parent opinion");
	}
	public void  gardening(){
		System.out.println("gardening");
    }
	public void takeRest(){
		System.out.println("taking rest by Parent");
	}
	protected void meetRelatives(){
		System.out.println("Meeting with Relatives");
	}
}