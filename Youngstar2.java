package chennai;
import salem.Parent;
public class Youngstar2 extends Parent{
	public Youngstar2(){
		System.out.println("Youngstar2 class constructor");
	}
	//public Youngstar2(int i){
	//	System.out.println("constructor 2:"+i);
	//}	
	public static void main(String[] args){
		Youngstar2 yy=new Youngstar2();// this yy cannot visible to outside of the block;
		//Youngstar2 xx=new Youngstar2(89);
		//yy acting as parent object also.
		yy.study();
		yy.gardening();
		//yy.savings();
		yy.fixMarriage();
		//System.out.println(yy.balance);
		//Parent pp=new Parent();
		//pp.fixMarriage();
		yy.takeRest();
		yy.visit();
		yy.meetRelatives();
		//super.takeRest(); super cannot be used in the static content.
	}
	public void study(){
		System.out.println("study");
	}
	public void fixMarriage(){
		System.out.println(super.balance);
		super.fixMarriage();
		System.out.println("Youngstar2-opinion");
	}
}